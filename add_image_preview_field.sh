#!/bin/bash

# Directory containing the JSON files
DIRECTORY="."

# Iterate through all .json files in the specified directory
for file in "$DIRECTORY"/*.json; do
  echo "Processing $file"

  # Use jq to add the imagePreview field by copying the image field
  jq '. + {imagePreview: .image}' "$file" > temp.json && mv temp.json "$file"

  echo "$file updated"
done
